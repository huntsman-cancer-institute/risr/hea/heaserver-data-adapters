#!/usr/bin/env python3

from heaserver.dataadapter import service
from heaserver.service import swaggerui
from integrationtests.heaserver.dataadapterintegrationtest.dataadaptertestcase import db_store
import logging

logging.basicConfig(level=logging.DEBUG)


if __name__ == '__main__':
    swaggerui.run('heaserver-data-adapters', db_store, service, [('/dataadapters/{id}', service.get_data_adapter)])
